# lox-wasm

wasm bindings for the lox crate: https://git-crysp.uwaterloo.ca/iang/lox

# Dependencies

```
cargo install wasm-pack
```

# Build

```
wasm-pack build --target web
```

# Testing

The provided `index.html` file can be used for testing the lox bindings. First, follow the instructions to [run a lox server](https://gitlab.torproject.org/cohosh/lox-server).

Then, spin up a simple local webserver in the current directory:
```
python3 -m http.server 8000
```

Next, open the dev console in your browser and navigate to `http://localhost:8000`.
