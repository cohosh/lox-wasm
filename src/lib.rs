use chrono::{Duration, Utc, NaiveDate};
use std::sync::atomic::{AtomicI64, Ordering};
use julianday::JulianDay;
use lazy_static::lazy_static;
use lox::bridge_table::{BridgeLine,from_scalar,BridgeTable, ENC_BUCKET_BYTES};
use lox::cred::{BucketReachability, Invitation, Lox, Migration};
use lox::proto::{open_invite, trust_promotion, migration, level_up,
    issue_invite, redeem_invite, check_blockage, blockage_migration};
use lox::{IssuerPubKey, OPENINV_LENGTH, scalar_u32};
use serde::{Deserialize, Serialize};
use serde_with::{serde_as};
use serde_json;
//use serde_wasm_bindgen;
use std::array::TryFromSliceError;
use std::{panic};
use wasm_bindgen::prelude::*;
use zkp::ProofError;


#[derive(Deserialize, Serialize)]
struct OpenReqState {
    request: open_invite::Request,
    state: open_invite::State,
}

#[derive(Deserialize, Serialize)]
struct TrustReqState {
    request: trust_promotion::Request,
    state: trust_promotion::State,
}

#[derive(Deserialize, Serialize)]
struct MigReqState {
    request: migration::Request,
    state: migration::State,
}

#[derive(Deserialize, Serialize)]
struct LevelupReqState {
    request: level_up::Request,
    state: level_up::State,
}

#[derive(Deserialize, Serialize)]
struct IssueInviteReqState {
    request: issue_invite::Request,
    state: issue_invite::State,
}

#[derive(Deserialize, Serialize)]
struct RedeemReqState {
    request: redeem_invite::Request,
    state: redeem_invite::State,
}

#[derive(Deserialize, Serialize)]
struct CheckBlockageReqState {
    request: check_blockage::Request,
    state: check_blockage::State,
}

#[derive(Deserialize, Serialize)]
struct BlockageMigReqState {
    request: blockage_migration::Request,
    state: blockage_migration::State,
}



#[derive(Debug, Deserialize, Serialize)]
struct PubKeys {
    lox_pub: IssuerPubKey,
    migration_pub: IssuerPubKey,
    migrationkey_pub: IssuerPubKey,
    reachability_pub: IssuerPubKey,
    invitation_pub: IssuerPubKey,
}

#[serde_as]
#[derive(Serialize, Deserialize)]
pub struct EncBridgeTable {
   #[serde_as(as = "Vec<[_; ENC_BUCKET_BYTES]>")]
    pub etable: Vec<[u8; ENC_BUCKET_BYTES]>,
}

#[derive(Debug, Deserialize, Serialize)]
struct LoxCredential {
    lox_credential: Lox,
    bridgeline: Option<BridgeLine>,
    invitation: Option<Invitation>,
}

fn today() -> u32 {
    let naive_now = Utc::now().date_naive();
    JulianDay::from(naive_now).inner().try_into().unwrap()
}

// This should only be used for testing, use today in production
fn test_today(days: i64) -> u32 {
    let naive_now_plus = (Utc::now() + Duration::days(days)).date_naive();
    JulianDay::from(naive_now_plus).inner().try_into().unwrap()
}


//pub const MAX_LEVEL: usize = 4;
//pub const LEVEL_INTERVAL: [u32; MAX_LEVEL + 1] = [0, 14, 28, 56, 84];
fn calc_test_days(lox_cred: &Lox) -> Result<i64, ProofError> {
    let trust_level: i64 = match scalar_u32(&lox_cred.trust_level) {
        Some(v) => v as i64,
        None => return Err(ProofError::VerificationFailure),
            };
    let mut total = 31;
   // for level in 0..trust_level {
  //      let level_interval: u32 = LEVEL_INTERVAL[trust_level as usize];
  //     total += level_interval;
        total += trust_level*85;
  //  }
    Ok(total)
    }

#[wasm_bindgen]
extern "C" {
    #[wasm_bindgen(js_namespace = console)]
    pub fn log(s: &str);
}


#[wasm_bindgen]
pub fn set_panic_hook() {
    panic::set_hook(Box::new(console_error_panic_hook::hook));
}

#[wasm_bindgen]
pub fn open_invite(invite: &[u8]) -> Result<String, JsValue> {
    unsafe {
        log(&format!("Using invite: {:?}", invite));
    }
    let token = match validate(invite) {
        Ok(token) => token,
        Err(e) => return Err(JsValue::from(e.to_string())),
    };
    let (request, state) = open_invite::request(&token);
    let req_state = OpenReqState {
        request: request,
        state: state,
    };
    unsafe {
        log(&format!(
            "Formatted open invite request: {}",
            serde_json::to_string(&req_state).unwrap()
        ));
    }
    Ok(serde_json::to_string(&req_state).unwrap())
}

#[wasm_bindgen]
pub fn handle_new_lox_credential(
    open_lox_result: String,
    open_lox_response: String,
    lox_pub: String,
) -> Result<String, JsValue> {
    let req_state: OpenReqState = serde_json::from_str(&open_lox_result).unwrap();
    let deserialized_state = req_state.state;
    let deserialized_response = serde_json::from_str(&open_lox_response).unwrap();
    let pubkeys: PubKeys = serde_json::from_str(&lox_pub).unwrap();
    let lox_cred = match open_invite::handle_response(
        deserialized_state,
        deserialized_response,
        &pubkeys.lox_pub,
    ) {
        Ok(lox_cred) => lox_cred,
        Err(e) => {
            log(&format!("Error: {:?}", e.to_string()));
            return Err(JsValue::from(e.to_string()));
        }
    };
    let lox_cred = LoxCredential {
        lox_credential: lox_cred.0,
        bridgeline: Some(lox_cred.1),
        invitation: None,
    };
    unsafe {
        log(&format!(
            "Got new Lox Credential: {}",
            serde_json::to_string(&lox_cred.lox_credential).unwrap()
        ));
        log(&format!(
            "Got new bridgeline: {}",
            serde_json::to_string(&lox_cred.bridgeline).unwrap()
        ));
    }
    Ok(serde_json::to_string(&lox_cred).unwrap())
}

#[wasm_bindgen]
pub fn trust_promotion(open_lox_cred: String, lox_pub: String) -> Result<String, JsValue> {
    let lox_cred: LoxCredential = serde_json::from_str(&open_lox_cred).unwrap();
    let pubkeys: PubKeys = serde_json::from_str(&lox_pub).unwrap();
    // To test creation of the credential we need to advance the day to 30
    // in production this should just use the today() function
    log(&format!(
        "TEST ONLY: Add 31 days to today's date: {}",
        test_today(31)
    ));
    let tp_result =
        //CHANGE add_today(31) to today() for production
        match trust_promotion::request(&lox_cred.lox_credential, &pubkeys.lox_pub, test_today(31)) {
            Ok(tp_result) => tp_result,
            Err(e) => {
                log(&format!("Error: {:?}", e.to_string()));
                return Err(JsValue::from(e.to_string()));
            }
        };
    let req_state = TrustReqState {
        request: tp_result.0,
        state: tp_result.1,
    };
    unsafe {
        log(&format!(
            "Formatted Trust Promotion request: {}",
            serde_json::to_string(&req_state).unwrap()
        ));
    }
    Ok(serde_json::to_string(&req_state).unwrap())
}

#[wasm_bindgen]
pub fn handle_trust_promotion(
    trust_promo_request: String,
    trust_promo_response: String,
) -> Result<String, JsValue> {
    let req_state: TrustReqState = serde_json::from_str(&trust_promo_request).unwrap();
    let deserialized_state = req_state.state;
    let deserialized_response = serde_json::from_str(&trust_promo_response).unwrap();
    let migration_cred =
        match trust_promotion::handle_response(deserialized_state, deserialized_response) {
            Ok(migration_cred) => migration_cred,
            Err(e) => {
                log(&format!("Error: {:?}", e.to_string()));
                return Err(JsValue::from(e.to_string()));
            }
        };
    unsafe {
        log(&format!(
            "Got new Migration Credential: {}",
            serde_json::to_string(&migration_cred).unwrap()
        ));
    }
    Ok(serde_json::to_string(&migration_cred).unwrap())
}

#[wasm_bindgen]
pub fn trust_migration(open_lox_cred: String, trust_promo_cred: String, lox_pub: String) -> Result<String, JsValue> {
    let lox_cred: LoxCredential = serde_json::from_str(&open_lox_cred).unwrap();
    let pubkeys: PubKeys = serde_json::from_str(&lox_pub).unwrap();
    let mig_cred: Migration = serde_json::from_str(&trust_promo_cred).unwrap();
    let tm_result =
        match migration::request(&lox_cred.lox_credential, &mig_cred, &pubkeys.lox_pub, &pubkeys.migration_pub) {
            Ok(tm_result) => tm_result,
            Err(e) => {
                log(&format!("Error: {:?}", e.to_string()));
                return Err(JsValue::from(e.to_string()));
            }
        };
    let req_state = MigReqState {
        request: tm_result.0,
        state: tm_result.1,
    };
    unsafe {
        log(&format!(
            "Formatted Trust Migration request: {}",
            serde_json::to_string(&req_state).unwrap()
        ));
    }
    Ok(serde_json::to_string(&req_state).unwrap())
}

#[wasm_bindgen]
pub fn handle_trust_migration(
    trust_migration_request: String,
    trust_migration_response: String,
    lox_pub: String
) -> Result<String, JsValue> {
    let pubkeys: PubKeys = serde_json::from_str(&lox_pub).unwrap();
    let req_state: MigReqState = serde_json::from_str(&trust_migration_request).unwrap();
    let deserialized_state = req_state.state;
    let deserialized_response = serde_json::from_str(&trust_migration_response).unwrap();
    let level_one_cred =
        match migration::handle_response(deserialized_state, deserialized_response, &pubkeys.lox_pub) {
            Ok(level_1_cred) => LoxCredential {
                lox_credential: level_1_cred,
                bridgeline: None,
                invitation: None,
            },
            Err(e) => {
                log(&format!("Error: {:?}", e.to_string()));
                return Err(JsValue::from(e.to_string()));
            }
        };
    unsafe {
        log(&format!(
            "Got new Level 1 Credential: {}",
            serde_json::to_string(&level_one_cred).unwrap()
        ));
    }
    Ok(serde_json::to_string(&level_one_cred).unwrap())
}

fn generate_reachability_cred(lox_cred: &Lox, encrypted_table: String) -> BucketReachability {
    let (id, key) = from_scalar(lox_cred.bucket).unwrap();
    let enc_buckets: EncBridgeTable = serde_json::from_str(&encrypted_table).unwrap();
    let bucket = BridgeTable::decrypt_bucket(id, &key, &enc_buckets.etable[id as usize]).unwrap();
    return bucket.1.unwrap()
}

#[wasm_bindgen]
pub fn level_up(level_one_cred: String, encrypted_table: String, lox_pub: String) -> Result<String, JsValue> {
    let lox_cred: LoxCredential = serde_json::from_str(&level_one_cred).unwrap();
    let pubkeys: PubKeys = serde_json::from_str(&lox_pub).unwrap();
    let reach_cred = generate_reachability_cred(&lox_cred.lox_credential, encrypted_table);

    // To test level up of the credential we need to advance the day to the correct interval
    // In this case, the maximum of 85 can be used to test all level ups
    // in production this should just use the today() function
    // decrypt trust level and use to calculate the correct date for now
        // The trust level has to be at least 1
    let test_cumulative_days = match calc_test_days(&lox_cred.lox_credential) {
        Ok(v) => v,
        Err(e) => {
            log(&format!("Error: {:?}", e.to_string()));
            return Err(JsValue::from(e.to_string()));
        }
    };

    log(&format!(
        "TEST ONLY: Add 31 (open invitation) + Trust Level*85 days to today's date: {}",  test_today(test_cumulative_days)
    ));
    let lu_result =
        //CHANGE add_today(31) to today() for production
        match level_up::request(&lox_cred.lox_credential, &reach_cred, &pubkeys.lox_pub, &pubkeys.reachability_pub, test_today(test_cumulative_days)) {
            Ok(lu_result) => lu_result,
            Err(e) => {
                log(&format!("Error: {:?}", e.to_string()));
                return Err(JsValue::from(e.to_string()));
            }
        };
    let req_state = LevelupReqState {
        request: lu_result.0,
        state: lu_result.1,
    };
    unsafe {
        log(&format!(
            "Formatted Level Up request: {}",
            serde_json::to_string(&req_state).unwrap()
        ));
    }
    Ok(serde_json::to_string(&req_state).unwrap())
}


#[wasm_bindgen]
pub fn handle_level_up(
    levelup_request: String,
    levelup_response: String,
    lox_pub: String
) -> Result<String, JsValue> {
    let pubkeys: PubKeys = serde_json::from_str(&lox_pub).unwrap();
    let req_state: LevelupReqState = serde_json::from_str(&levelup_request).unwrap();
    let deserialized_state = req_state.state;
    let deserialized_response = serde_json::from_str(&levelup_response).unwrap();
    let level_up_cred =
        match level_up::handle_response(deserialized_state, deserialized_response, &pubkeys.lox_pub) {
            Ok(level_up_cred) => LoxCredential {
                lox_credential: level_up_cred,
                bridgeline: None,
                invitation: None,
            },
            Err(e) => {
                log(&format!("Error: {:?}", e.to_string()));
                return Err(JsValue::from(e.to_string()));
            }
        };
    unsafe {
        log(&format!(
            "Got new Level Up Credential: {}",
            serde_json::to_string(&level_up_cred).unwrap()
        ));
    }
    Ok(serde_json::to_string(&level_up_cred).unwrap())
}

#[wasm_bindgen]
pub fn issue_invite(trusted_cred: String, encrypted_table: String, lox_pub: String) -> Result<String, JsValue> {
    let lox_cred: LoxCredential = serde_json::from_str(&trusted_cred).unwrap();
    let pubkeys: PubKeys = serde_json::from_str(&lox_pub).unwrap();
    let reach_cred = generate_reachability_cred(&lox_cred.lox_credential, encrypted_table);

    let issue_result =
        match issue_invite::request(&lox_cred.lox_credential, &reach_cred, &pubkeys.lox_pub, &pubkeys.reachability_pub, test_today(371)) {
            Ok(issue_result) => issue_result,
            Err(e) => {
                log(&format!("Error: {:?}", e.to_string()));
                return Err(JsValue::from(e.to_string()));
            }
        };
    let req_state = IssueInviteReqState {
        request: issue_result.0,
        state: issue_result.1,
    };
    unsafe {
        log(&format!(
            "Formatted Issue Invite request: {}",
            serde_json::to_string(&req_state).unwrap()
        ));
    }
    Ok(serde_json::to_string(&req_state).unwrap())
}


#[wasm_bindgen]
pub fn handle_issue_invite(
    issue_invite_request: String,
    issue_invite_response: String,
    lox_pub: String
) -> Result<String, JsValue> {
    let pubkeys: PubKeys = serde_json::from_str(&lox_pub).unwrap();
    let req_state: IssueInviteReqState = serde_json::from_str(&issue_invite_request).unwrap();
    let deserialized_state = req_state.state;
    let deserialized_response = serde_json::from_str(&issue_invite_response).unwrap();
    let issue_invite_cred =
        match issue_invite::handle_response(deserialized_state, deserialized_response, &pubkeys.lox_pub, &pubkeys.invitation_pub) {
            Ok(issue_invite_cred) => issue_invite_cred,
            Err(e) => {
                log(&format!("Error: {:?}", e.to_string()));
                return Err(JsValue::from(e.to_string()));
            }
        };
        let invitation_cred = LoxCredential {
            lox_credential: issue_invite_cred.0,
            bridgeline: None,
            invitation: Some(issue_invite_cred.1),
        };

    unsafe {
        log(&format!(
            "Got new Invitation Credential and Lox Credential: {}",
            serde_json::to_string(&invitation_cred).unwrap()
        ));
    }
    Ok(serde_json::to_string(&invitation_cred).unwrap())
}

// Separate Trusted Invite from credential prior to passing it to friend
#[wasm_bindgen]
pub fn prepare_invite(invitation_cred: String) -> String {
    let cred: LoxCredential = serde_json::from_str(&invitation_cred).unwrap();
    log(&format!(
        "Prepared Invitation: {}",
        serde_json::to_string(&cred.invitation).unwrap()
    ));
    serde_json::to_string(&cred.invitation).unwrap()
}

//
#[wasm_bindgen]
pub fn redeem_invite(invitation: String, lox_pub: String) -> Result<String, JsValue> {
    let invitation_cred: Invitation = serde_json::from_str(&invitation).unwrap();
    let pubkeys: PubKeys = serde_json::from_str(&lox_pub).unwrap();
    let redeem_result =
        match redeem_invite::request(&invitation_cred, &pubkeys.invitation_pub, test_today(371)) {
            Ok(redeem_result) => redeem_result,
            Err(e) => {
                log(&format!("Error: {:?}", e.to_string()));
                return Err(JsValue::from(e.to_string()));
            }
        };
    let req_state = RedeemReqState {
        request: redeem_result.0,
        state: redeem_result.1,
    };
    unsafe {
        log(&format!(
            "Formatted Redeem Invite request: {}",
            serde_json::to_string(&req_state).unwrap()
        ));
    }
    Ok(serde_json::to_string(&req_state).unwrap())
}

#[wasm_bindgen]
pub fn handle_redeem_invite(
    redeem_invite_request: String,
    redeem_invite_response: String,
    lox_pub: String
) -> Result<String, JsValue> {
    let pubkeys: PubKeys = serde_json::from_str(&lox_pub).unwrap();
    let req_state: RedeemReqState = serde_json::from_str(&redeem_invite_request).unwrap();
    let deserialized_state = req_state.state;
    let deserialized_response = serde_json::from_str(&redeem_invite_response).unwrap();
    let redeem_invite_cred =
        match redeem_invite::handle_response(deserialized_state, deserialized_response, &pubkeys.lox_pub) {
            Ok(issue_invite_cred) => LoxCredential {
                lox_credential: issue_invite_cred,
                bridgeline: None,
                invitation: None,
            },
            Err(e) => {
                log(&format!("Error: {:?}", e.to_string()));
                return Err(JsValue::from(e.to_string()));
            }
        };
    unsafe {
        log(&format!(
            "Got new Trusted Lox Credential from Invitation: {}",
            serde_json::to_string(&redeem_invite_cred).unwrap()
        ));
    }
    Ok(serde_json::to_string(&redeem_invite_cred).unwrap())
}


#[wasm_bindgen]
pub fn check_blockage(lox_cred: String, lox_pub: String) -> Result<String, JsValue> {
    let lox: LoxCredential = serde_json::from_str(&lox_cred).unwrap();
    let pubkeys: PubKeys = serde_json::from_str(&lox_pub).unwrap();
    let cb_result =
        match check_blockage::request(&lox.lox_credential, &pubkeys.lox_pub) {
            Ok(cb_result) => cb_result,
            Err(e) => {
                log(&format!("Error: {:?}", e.to_string()));
                return Err(JsValue::from(e.to_string()));
            }
        };
    let req_state = CheckBlockageReqState {
        request: cb_result.0,
        state: cb_result.1,
    };
    unsafe {
        log(&format!(
            "Formatted Check Blockage request: {}",
            serde_json::to_string(&req_state).unwrap()
        ));
    }
    Ok(serde_json::to_string(&req_state).unwrap())
}

#[wasm_bindgen]
pub fn handle_check_blockage(
    check_blockage_request: String,
    check_blockage_response: String,
) -> Result<String, JsValue> {
    let req_state: CheckBlockageReqState = serde_json::from_str(&check_blockage_request).unwrap();
    let deserialized_state = req_state.state;
    let deserialized_response = serde_json::from_str(&check_blockage_response).unwrap();
    let migration_cred =
        match check_blockage::handle_response(deserialized_state, deserialized_response) {
            Ok(migration_cred) => migration_cred,
            Err(e) => {
                log(&format!("Error: {:?}", e.to_string()));
                return Err(JsValue::from(e.to_string()));
            }
        };
    unsafe {
        log(&format!(
            "Got new Blockage Migration Credential: {}",
            serde_json::to_string(&migration_cred).unwrap()
        ));
    }
    Ok(serde_json::to_string(&migration_cred).unwrap())
}


#[wasm_bindgen]
pub fn blockage_migration(lox_cred: String, check_migration_cred: String, lox_pub: String) -> Result<String, JsValue> {
    let lox_cred: LoxCredential = serde_json::from_str(&lox_cred).unwrap();
    let pubkeys: PubKeys = serde_json::from_str(&lox_pub).unwrap();
    let mig_cred: Migration = serde_json::from_str(&check_migration_cred).unwrap();
    let bm_result =
        match blockage_migration::request(&lox_cred.lox_credential, &mig_cred, &pubkeys.lox_pub, &pubkeys.migration_pub) {
            Ok(bm_result) => bm_result,
            Err(e) => {
                log(&format!("Error: {:?}", e.to_string()));
                return Err(JsValue::from(e.to_string()));
            }
        };
    let req_state = BlockageMigReqState {
        request: bm_result.0,
        state: bm_result.1,
    };
    unsafe {
        log(&format!(
            "Formatted Blockage Migration request: {}",
            serde_json::to_string(&req_state).unwrap()
        ));
    }
    Ok(serde_json::to_string(&req_state).unwrap())
}

#[wasm_bindgen]
pub fn handle_blockage_migration(
    blockage_migration_request: String,
    blockage_migration_response: String,
    lox_pub: String
) -> Result<String, JsValue> {
    let pubkeys: PubKeys = serde_json::from_str(&lox_pub).unwrap();
    let req_state: BlockageMigReqState = serde_json::from_str(&blockage_migration_request).unwrap();
    let deserialized_state = req_state.state;
    let deserialized_response = serde_json::from_str(&blockage_migration_response).unwrap();
    let lox_cred =
        match blockage_migration::handle_response(deserialized_state, deserialized_response, &pubkeys.lox_pub) {
            Ok(lox_cred) => LoxCredential {
                lox_credential: lox_cred,
                bridgeline: None,
                invitation: None,
            },
            Err(e) => {
                log(&format!("Error: {:?}", e.to_string()));
                return Err(JsValue::from(e.to_string()));
            }
        };
    unsafe {
        log(&format!(
            "Got new Lox Credential after Migration: {}",
            serde_json::to_string(&lox_cred).unwrap()
        ));
    }
    Ok(serde_json::to_string(&lox_cred).unwrap())
}

// This should also check the pubkey
fn validate(invite: &[u8]) -> Result<[u8; OPENINV_LENGTH], TryFromSliceError> {
    invite.try_into()
}
